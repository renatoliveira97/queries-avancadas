SELECT * FROM enderecos;

SELECT * 
FROM enderecos e
JOIN
    usuarios u
    ON u.endereco_id = e.id;

SELECT r.id, r.nome, u.id, u.nome, u.email, u.senha, u.endereco_id
FROM redes_sociais r
JOIN
    usuario_rede_sociais urs
	ON urs.rede_social_id = r.id 
JOIN
	usuarios u 
	on urs.usuario_id = u.id;

SELECT r.id, r.nome, u.id, u.nome, u.email, u.senha, u.endereco_id, e.id, e.rua, e.pais, e.cidade
FROM redes_sociais r
JOIN 
	usuario_rede_sociais urs 
	ON urs.rede_social_id = r.id 
JOIN
    usuarios u
    ON urs.usuario_id = u.id
JOIN
    enderecos e
    ON u.endereco_id = e.id;

SELECT r.nome AS rede_social, u.nome AS usuario, u.email, e.cidade
FROM redes_sociais r
JOIN 
	usuario_rede_sociais urs 
	ON urs.rede_social_id = r.id 
JOIN
    usuarios u
    ON urs.usuario_id = u.id
JOIN
    enderecos e
    ON u.endereco_id = e.id;

SELECT r.nome AS rede_social, u.nome AS usuario, u.email, e.cidade
FROM redes_sociais r
JOIN 
	usuario_rede_sociais urs 
	ON urs.rede_social_id = r.id 
JOIN
    usuarios u
    ON urs.usuario_id = u.id
JOIN
    enderecos e
    ON u.endereco_id = e.id
WHERE r.nome = 'Youtube';

SELECT r.nome AS rede_social, u.nome AS usuario, u.email, e.cidade
FROM redes_sociais r
JOIN 
	usuario_rede_sociais urs 
	ON urs.rede_social_id = r.id 
JOIN
    usuarios u
    ON urs.usuario_id = u.id
JOIN
    enderecos e
    ON u.endereco_id = e.id
WHERE r.nome = 'Instagram';

SELECT r.nome AS rede_social, u.nome AS usuario, u.email, e.cidade
FROM redes_sociais r
JOIN 
	usuario_rede_sociais urs 
	ON urs.rede_social_id = r.id 
JOIN
    usuarios u
    ON urs.usuario_id = u.id
JOIN
    enderecos e
    ON u.endereco_id = e.id
WHERE r.nome = 'Facebook';

SELECT r.nome AS rede_social, u.nome AS usuario, u.email, e.cidade
FROM redes_sociais r
JOIN 
	usuario_rede_sociais urs 
	ON urs.rede_social_id = r.id 
JOIN
    usuarios u
    ON urs.usuario_id = u.id
JOIN
    enderecos e
    ON u.endereco_id = e.id
WHERE r.nome = 'TikTok';

SELECT r.nome AS rede_social, u.nome AS usuario, u.email, e.cidade
FROM redes_sociais r
JOIN 
	usuario_rede_sociais urs 
	ON urs.rede_social_id = r.id 
JOIN
    usuarios u
    ON urs.usuario_id = u.id
JOIN
    enderecos e
    ON u.endereco_id = e.id
WHERE r.nome = 'Twitter';
